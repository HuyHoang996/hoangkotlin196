package com.example.kotlinapp.handle

interface ApiHandle<T> {
	fun onSuccess(t: T)
	fun onFailed(e: Throwable)
}