package com.example.kotlinapp.service.calbacks

import com.example.kotlinapp.base.callbacks.BaseResponse
import com.example.kotlinapp.userlogin.UserLogin
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Multipart

interface ApiService {
    @Multipart
    @POST
    suspend fun login(@Body userLogin: UserLogin) : Response<BaseResponse<UserLogin>>
}