package com.example.kotlinapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinapp.R
import com.example.kotlinapp.base.BaseFragment

class HomeFragment : BaseFragment() {
	private lateinit var homeViewModel: HomeViewModel
	override fun getContentView(): Int {
		return R.layout.fragment_home
	}

	override fun onViewReady(savedInstanceState: Bundle?, bundle: Bundle?) {

	}

}