package com.example.kotlinapp.ui.plans

import android.os.Bundle
import com.example.kotlinapp.R
import com.example.kotlinapp.base.BaseFragment

class PlansFragment : BaseFragment() {
	private lateinit var galleryViewModel: PlanViewModel

	override fun getContentView(): Int {
		return R.layout.fragment_plans
	}

	override fun onViewReady(savedInstanceState: Bundle?, bundle: Bundle?) {

	}
}