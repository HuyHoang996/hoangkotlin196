package com.example.kotlinapp.base.callbacks

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BaseResponse<T> : Serializable {
    @SerializedName("statusCode")
    var statusCode = 0
    @SerializedName("succeeded")
    var isSucceeded = false
    @SerializedName("message")
    var messageCode: String? = null
    @SerializedName("data")
    var data: T? = null
        private set

    fun setData(data: T) {
        this.data = data
    }
}