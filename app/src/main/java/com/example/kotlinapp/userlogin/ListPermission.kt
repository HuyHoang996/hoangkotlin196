package com.example.kotlinapp.userlogin

import com.google.gson.annotations.SerializedName

class ListPermission {
	@SerializedName("id")
	var id: String? = null
	@SerializedName("name")
	var name: String? = null
	@SerializedName("policy")
	var policy: String? = null
}