package com.example.kotlinapp.userlogin

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserInfomation {
	@SerializedName("id")
	@Expose
	var id: String? = null
	@SerializedName("hoTen")
	@Expose
	var hoTen: String? = null
	@SerializedName("anhDaiDien_FilePath")
	@Expose
	var anhDaiDien: String? = null
	@SerializedName("chucVu")
	@Expose
	var chucVu: String? = null
	@SerializedName("donVi")
	@Expose
	var donVi: String? = null
	@SerializedName("email")
	@Expose
	var email: String? = null
	@SerializedName("userId")
	@Expose
	var userId: String? = null
	@SerializedName("userName")
	@Expose
	var userName: String? = null
	@SerializedName("donViId")
	@Expose
	var donViId: String? = null


}