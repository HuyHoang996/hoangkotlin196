package com.example.kotlinapp.userlogin

import com.google.gson.annotations.SerializedName

class ListRole {
	@SerializedName("id")
	var id: String? = null
	@SerializedName("name")
	var name: String? = null
}