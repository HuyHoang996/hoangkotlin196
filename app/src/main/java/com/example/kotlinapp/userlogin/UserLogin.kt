package com.example.kotlinapp.userlogin


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserLogin(@field:Expose @field:SerializedName("userName") var userName: String, @field:Expose @field:SerializedName("password") var password: String) {
	@SerializedName("userInformation")
	var userInfomation: UserInfomation? = null
	@SerializedName("listPermissions")
	var listPermissions: List<ListPermission>? = null
	@SerializedName("listRoles")
	var listRoles: List<ListRole>? = null
	@SerializedName("accessToken")
	var accessToken: String? = null
	@SerializedName("refreshToken")
	var refreshToken: String? = null



	override fun toString(): String {
		return "UserLogin{" +
				"userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", userInfomation=" + userInfomation +
				", listPermissions=" + listPermissions +
				", listRoles=" + listRoles +
				", accessToken='" + accessToken + '\'' +
				", refreshToken='" + refreshToken + '\'' +
				'}'
	}
}