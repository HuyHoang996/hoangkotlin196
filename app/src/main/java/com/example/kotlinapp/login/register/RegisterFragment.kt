package com.example.kotlinapp.login.register

import android.os.Bundle
import com.example.kotlinapp.R
import com.example.kotlinapp.base.BaseFragment

class RegisterFragment : BaseFragment(){
    override fun getContentView(): Int {
        return R.layout.fragment_register
    }

    override fun onViewReady(savedInstanceState: Bundle?, bundle: Bundle?) {

    }
}