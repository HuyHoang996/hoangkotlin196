package com.example.kotlinapp.login

import androidx.lifecycle.ViewModel
import com.example.kotlinapp.service.RetrofitClient

class LoginViewModel(private val listener: LoginResultCallBacks) : ViewModel() {

    fun onClickedLogin(userName: String, password: String) {

    }

    interface LoginResultCallBacks {
        fun onSuccess(token: String)
        fun onError(message: String)
    }
}